﻿#pragma checksum "..\..\..\Design\SubjectWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "D6965E351B61F493BE990C8F75DD1374"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using SchoolDiary1.Design;
using SchoolDiary1.Model;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SchoolDiary1.Design {
    
    
    /// <summary>
    /// SubjectWindow
    /// </summary>
    public partial class SubjectWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 15 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stackPanel1;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AddButton;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DeleteButton;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button EditButton;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel StackPanel2;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox subjectTextBox;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stackPanel3;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button saveButton;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button clearButton;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button cancelButton;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel searchStackPanel;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label1;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox searchTextBox;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button searchButton;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label2;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label4;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox subjectsComboBox;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label3_Copy;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox personComboBox2;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AssignButton;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button NoAssignButton;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\Design\SubjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ClearButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SchoolDiary1;component/design/subjectwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Design\SubjectWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.stackPanel1 = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 2:
            this.AddButton = ((System.Windows.Controls.Button)(target));
            
            #line 16 "..\..\..\Design\SubjectWindow.xaml"
            this.AddButton.Click += new System.Windows.RoutedEventHandler(this.AddButton_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.DeleteButton = ((System.Windows.Controls.Button)(target));
            
            #line 17 "..\..\..\Design\SubjectWindow.xaml"
            this.DeleteButton.Click += new System.Windows.RoutedEventHandler(this.DeleteButton_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.EditButton = ((System.Windows.Controls.Button)(target));
            
            #line 18 "..\..\..\Design\SubjectWindow.xaml"
            this.EditButton.Click += new System.Windows.RoutedEventHandler(this.EditButton_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.StackPanel2 = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 6:
            this.label = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.subjectTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.stackPanel3 = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 9:
            this.saveButton = ((System.Windows.Controls.Button)(target));
            
            #line 27 "..\..\..\Design\SubjectWindow.xaml"
            this.saveButton.Click += new System.Windows.RoutedEventHandler(this.saveButton_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.clearButton = ((System.Windows.Controls.Button)(target));
            
            #line 28 "..\..\..\Design\SubjectWindow.xaml"
            this.clearButton.Click += new System.Windows.RoutedEventHandler(this.clearButton_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.cancelButton = ((System.Windows.Controls.Button)(target));
            
            #line 29 "..\..\..\Design\SubjectWindow.xaml"
            this.cancelButton.Click += new System.Windows.RoutedEventHandler(this.cancelButton_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.searchStackPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 13:
            this.label1 = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.searchTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 33 "..\..\..\Design\SubjectWindow.xaml"
            this.searchTextBox.IsMouseCapturedChanged += new System.Windows.DependencyPropertyChangedEventHandler(this.searchTextBox_IsMouseCapturedChanged);
            
            #line default
            #line hidden
            return;
            case 15:
            this.searchButton = ((System.Windows.Controls.Button)(target));
            
            #line 34 "..\..\..\Design\SubjectWindow.xaml"
            this.searchButton.Click += new System.Windows.RoutedEventHandler(this.searchButton_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.label2 = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.label4 = ((System.Windows.Controls.Label)(target));
            return;
            case 18:
            this.subjectsComboBox = ((System.Windows.Controls.ComboBox)(target));
            
            #line 38 "..\..\..\Design\SubjectWindow.xaml"
            this.subjectsComboBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.classComboBox_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 19:
            this.label3_Copy = ((System.Windows.Controls.Label)(target));
            return;
            case 20:
            this.personComboBox2 = ((System.Windows.Controls.ComboBox)(target));
            
            #line 47 "..\..\..\Design\SubjectWindow.xaml"
            this.personComboBox2.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.personComboBox2_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 21:
            this.AssignButton = ((System.Windows.Controls.Button)(target));
            
            #line 49 "..\..\..\Design\SubjectWindow.xaml"
            this.AssignButton.Click += new System.Windows.RoutedEventHandler(this.AssignButton_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.NoAssignButton = ((System.Windows.Controls.Button)(target));
            
            #line 50 "..\..\..\Design\SubjectWindow.xaml"
            this.NoAssignButton.Click += new System.Windows.RoutedEventHandler(this.NoAssignButton_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            this.ClearButton = ((System.Windows.Controls.Button)(target));
            
            #line 51 "..\..\..\Design\SubjectWindow.xaml"
            this.ClearButton.Click += new System.Windows.RoutedEventHandler(this.ClearButton_Click_1);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

