﻿using SchoolDiary1.Design;
using SchoolDiary1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace SchoolDiary1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        object Teacher;
        object Guardian;
        object Student;
        public MainWindow()
        {
            InitializeComponent();
        }

        public MainWindow(object o)
        {
            InitializeComponent();

            if (o is Teachers)
                Teacher = o as Teachers;
            else if (o is Guardians)
                Guardian = o as Guardians;
            else if (o is Students)
                Student = o as Students;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                if (Guardian is Guardians)
                {
                    MessageBox.Show("Opiekun");
                }
                else if (Teacher is Teachers)
                {
                    MessageBox.Show("nauczyciel");
                }
                else if (Student is Students)
                {
                    MessageBox.Show("Uczeń");
                }
                else
                {
                    var teachers = context.Teachers
                        .Select(x => new { Imie = x.FirstName, Nazwisko = x.LastName, DrugieImie = x.MiddleName,
                        DataUrodzenia = x.BirthDate, Pesel = x.PESEL, NumerTelefonu = x.PhoneNumber,
                        Email = x.Email, Tytuł = x.Degree.ToString(), NumerDowodu = x.IdentityCardNumber});
                    dataGrid.ItemsSource = teachers.ToList();
                }
            }

        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            TeacherWindow teacherWindow = new TeacherWindow();
            teacherWindow.Show();
            this.Close();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            GuardianWindow guardianWindow = new GuardianWindow();
            guardianWindow.Show();
            this.Close();
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            StudentWindow studentWindow = new StudentWindow();
            studentWindow.Show();
            this.Close();
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            ClassWindows classWindow = new ClassWindows();
            classWindow.Show();
            this.Close();
        }

        private void MenuItem_Click_4(object sender, RoutedEventArgs e)
        {
            SubjectWindow subWindow = new SubjectWindow();
            subWindow.Show();
            this.Close();
        }

        private void MenuItem_Click_5(object sender, RoutedEventArgs e)
        {
            LoginWindow logWin = new LoginWindow();
            logWin.Show();
            this.Close();
        }
    }
}
