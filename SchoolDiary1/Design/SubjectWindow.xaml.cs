﻿using SchoolDiary1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SchoolDiary1.Design
{
    /// <summary>
    /// Interaction logic for Subject.xaml
    /// </summary>
    public partial class SubjectWindow : Window
    {
        bool buttonAddWasClicked = false;
        bool buttonDeleteWasClicked = false;
        bool buttonEditWasClicked = false;

        public SubjectWindow()
        {
            InitializeComponent();
            searchStackPanel.Visibility = Visibility.Hidden;
            saveButton.IsEnabled = clearButton.IsEnabled = StackPanel2.IsEnabled = false;


            personComboBox2.SelectedIndex = -1;
            subjectsComboBox.SelectedIndex = -1;

            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                List<string> teacherList = new List<string>();
                var teacher = context.Teachers.Where(x => x.PESEL != null).ToList();

                foreach (var item in teacher)
                    teacherList.Add(item.PESEL);

                personComboBox2.ItemsSource = teacherList;
            }
            personComboBox2.IsEnabled = true;


            subjectsComboBox.Items.Refresh();
            List<string> subjectsList = new List<string>();

            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var subjects = context.Subjects
                    .Where(x => x.SubjectName != null).ToList()
                    .OrderBy(x => x.SubjectName);

                foreach (var item in subjects)
                    subjectsList.Add(item.SubjectName);

                subjectsComboBox.ItemsSource = subjectsList;
            }
        }


        private void Enable()
        {
            saveButton.IsEnabled = clearButton.IsEnabled = StackPanel2.IsEnabled = true;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            buttonAddWasClicked = true;
            Enable();
            stackPanel1.IsEnabled = false;
        }
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            buttonDeleteWasClicked = true;
            Enable();
            stackPanel1.IsEnabled = false;
        }
        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            buttonEditWasClicked = true;
            searchStackPanel.Visibility = Visibility.Visible;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
                if (buttonAddWasClicked == true)
                    AddSubject();
                else if (buttonDeleteWasClicked == true)
                    DeleteSubject();
                else if (buttonEditWasClicked == true)
                    EditSubject();
        }

        private void AddSubject()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                Subjects subject = new Subjects()
                {
                    SubjectName = subjectTextBox.Text
                };

                var sub = context.Subjects.Where(x => x.SubjectName == subjectTextBox.Text).FirstOrDefault();

                if (sub != null)
                {
                    MessageBox.Show("Isnieje już przedmiot o takiej nazwie!");
                    subjectTextBox.Focus();
                }
                else
                {
                    context.Subjects.Add(subject);
                    context.SaveChanges();

                    MessageBox.Show("Przedmiot dodany!");

                    buttonAddWasClicked = false;
                    stackPanel1.IsEnabled = true;
                    StackPanel2.IsEnabled = false;
                    subjectTextBox.Text = null;
                    searchTextBox.Text = null;
                    saveButton.IsEnabled = false;
                    clearButton.IsEnabled = false;
                }
            }
        }
        private void DeleteSubject()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var subjectToDelete = context.Subjects.Where(x => x.SubjectName == subjectTextBox.Text).FirstOrDefault();

                if (subjectToDelete != null)
                {
                    context.Subjects.Remove(subjectToDelete);
                    context.SaveChanges();

                    MessageBox.Show("Przedmiot usunięty!");

                    buttonDeleteWasClicked = false;
                    subjectTextBox.Text = null;
                    stackPanel1.IsEnabled = true;
                    StackPanel2.IsEnabled = false;
                    saveButton.IsEnabled = false;
                    clearButton.IsEnabled = false;
                }
                else
                {
                    MessageBox.Show("Nie znaleziono przedmiotu o takiej nazwie!");
                    subjectTextBox.Focus();
                }
            }
        }
        private void EditSubject()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var subjectToEdit = context.Subjects.Where(x => x.SubjectName == searchTextBox.Text).SingleOrDefault();

                subjectToEdit.SubjectName = subjectTextBox.Text;

                context.SaveChanges();

                buttonEditWasClicked = false;
                subjectTextBox.Text = null;
                stackPanel1.IsEnabled = true;
                saveButton.IsEnabled = false;
                clearButton.IsEnabled = false;
                StackPanel2.IsEnabled = false;

                MessageBox.Show("Zaktualizowano!");
            }

        }

        private void searchTextBox_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            searchTextBox.Clear();
        }

        private void searchButton_Click(object sender, RoutedEventArgs e)
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var subjectToEdit = context.Subjects.Where(x => x.SubjectName == searchTextBox.Text).SingleOrDefault();

                if (subjectToEdit != null)
                {
                    searchStackPanel.Visibility = Visibility.Hidden;
                    StackPanel2.IsEnabled = true;

                    saveButton.IsEnabled = true;
                    clearButton.IsEnabled = true;

                    subjectTextBox.Text = subjectToEdit.SubjectName;
                }
                else
                {
                    MessageBox.Show("Nie znaleziono takiego przedmiotu!");
                    searchTextBox.Focus();
                }
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            subjectTextBox.Text = searchTextBox.Text = null;
            stackPanel1.IsEnabled = true;
            saveButton.IsEnabled = clearButton.IsEnabled = false;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void radioButton_Click(object sender, RoutedEventArgs e)
        {
            personComboBox2.SelectedIndex = -1;
            subjectsComboBox.SelectedIndex = -1;

            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                List<string> teacherList = new List<string>();
                var teacher = context.Teachers.Where(x => x.PESEL != null).ToList();

                foreach (var item in teacher)
                    teacherList.Add(item.PESEL);

                personComboBox2.ItemsSource = teacherList;
            }
            personComboBox2.IsEnabled = true;

           // AddClassToComboBox();
        }

        private void radioButton2_Click(object sender, RoutedEventArgs e)
        {
            personComboBox2.SelectedIndex = -1;
            subjectsComboBox.SelectedIndex = -1;

            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                List<string> studentList = new List<string>();
                var student = context.Students.Where(x => x.PESEL != null).ToList();

                foreach (var item in student)
                    studentList.Add(item.PESEL);

                personComboBox2.ItemsSource = studentList;
            }
            personComboBox2.IsEnabled = true;

            //AddClassToComboBox();
        }
        private void AddSubjectsToComboBox()
        {
            subjectsComboBox.Items.Refresh();
            List<string> subjectsList = new List<string>();

            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var subjects = context.Subjects
                    .Where(x => x.SubjectName != null).ToList()
                    .OrderBy(x => x.SubjectName);

                foreach (var item in subjects)
                    subjectsList.Add(item.SubjectName);

                subjectsComboBox.ItemsSource = subjectsList;
            }
        }

        private void ClearButton_Click_1(object sender, RoutedEventArgs e)
        {
            personComboBox2.SelectedIndex = subjectsComboBox.SelectedIndex = -1;
            personComboBox2.IsEnabled = subjectsComboBox.IsEnabled = false;
        }

        private void personComboBox2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            subjectsComboBox.IsEnabled = true;
        }

        private void classComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AssignButton.IsEnabled = NoAssignButton.IsEnabled = true;
        }

        private void AssignButton_Click(object sender, RoutedEventArgs e)
        {

                //if (StudentRadioButton.IsChecked == true)
                //{
                //    var student = context.Students.Where(x => x.PESEL == personComboBox2.SelectedItem.ToString()).FirstOrDefault();

                //    if (clas.Students.Contains(student))
                //        MessageBox.Show("Uczeń jest już przypisany do tek klasy!");
                //    else
                //    {
                //        clas.Students.Add(student);
                //        MessageBox.Show("Uczeń przypisany do klasy!");
                //    }
                //}
                //else if (TeacherRadioButton.IsChecked == true)
                //{
                //    var teacher = context.Teachers.Single(x => x.PESEL == personComboBox2.SelectedItem.ToString());


                //    if (teacher.Classes.Contains(clas))
                //        MessageBox.Show("Nauczyciel jest już przypisany do tej klasy!");
                //    else
                //    {
                //        teacher.Classes.Add(clas);
                //        MessageBox.Show("Nauczyciel przypisany do klasy!");
                //    }
                //}
                //context.SaveChanges();
        }

        private void NoAssignButton_Click(object sender, RoutedEventArgs e)
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
               
                //test
            }
        }
    }
}
