﻿using SchoolDiary1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SchoolDiary1.Design
{
    /// <summary>
    /// Interaction logic for GuardianWindow.xaml
    /// </summary>
    public partial class GuardianWindow : Window
    {
        bool buttonAddWasClicked = false;
        bool buttonDeleteWasClicked = false;
        bool buttonEditWasClicked = false;

        public GuardianWindow()
        {
            InitializeComponent();
            saveButton.IsEnabled = clearButton.IsEnabled = canvas1.IsEnabled = false;
            stackPanelSearch.Visibility = Visibility.Hidden;
        }

        private void EnableThings()
        {
            canvas1.IsEnabled = true;
            saveButton.IsEnabled = clearButton.IsEnabled = true;
        }
        private void DisableThings()
        {
            saveButton.IsEnabled = clearButton.IsEnabled = canvas1.IsEnabled = false;
        }
        private void ClearTextBox()
        {
            firstNameTextBox.Text = lastNameTextBox.Text = peselTextBox.Text = null;
        }
        private void VisibleOnDelete()
        {
            label.Visibility = label1.Visibility = firstNameTextBox.Visibility = lastNameTextBox.Visibility = Visibility.Visible;
        }
        private void VisibleOffDelete()
        {
            label.Visibility = label1.Visibility = firstNameTextBox.Visibility = lastNameTextBox.Visibility = Visibility.Hidden;
        }
        private void VisibleOnEdit()
        {
            stackPanelSearch.Visibility = Visibility.Visible;
            canvas1.IsEnabled = false;
        }
        private void VisibleOffEdit()
        {
            stackPanelSearch.Visibility = Visibility.Hidden;
            canvas1.IsEnabled = true;
            stackPanel2.IsEnabled = true;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            EnableThings();
            buttonAddWasClicked = true;
            stackPanel1.IsEnabled = false;
        }
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            EnableThings();
            buttonDeleteWasClicked = true;
            VisibleOffDelete();

        }
        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            EnableThings();
            VisibleOnEdit();
            buttonEditWasClicked = true;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            if (firstNameTextBox.Text.Length == 0 && lastNameTextBox.Text.Length == 0 && peselTextBox.Text.Length == 0)
                errorTextBlock.Text = "Wprowadź wszystkie dane!";
            else if (!Regex.IsMatch(peselTextBox.Text, "^[0-9]+$"))
            {
                errorTextBlock.Text = "Pesel musi zawierać tylko cyfry!";
                peselTextBox.Focus();
            }
            else if (peselTextBox.Text.Length != 11)
            {
                errorTextBlock.Text = "Pesel musi mieć 11 cyfr!";
                peselTextBox.Focus();
            }
            else
            {
                errorTextBlock.Text = "";
                if (buttonAddWasClicked == true)
                    AddGuardian();

                else if (buttonDeleteWasClicked == true)
                    DeleteGuardian();
                else if (buttonEditWasClicked == true)
                    EditGuardian();

            }
        }
        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            firstNameTextBox.Text = lastNameTextBox.Text = peselTextBox.Text = null;
        }
        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void AddGuardian()
        {
            EnableThings();
            saveButton.IsEnabled = clearButton.IsEnabled = true;
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                Guardians guardian = new Guardians()
                {
                    FirstName = firstNameTextBox.Text,
                    LastName = lastNameTextBox.Text,
                    PESEL = peselTextBox.Text
                };
                LoginsPasswords login = new LoginsPasswords()
                {
                    Login = peselTextBox.Text,
                    Password = null
                };

                var peselGuardian = context.Guardians.Where(x => x.PESEL == peselTextBox.Text).FirstOrDefault();
                var peselTeacher = context.Teachers.Where(x => x.PESEL == peselTextBox.Text).FirstOrDefault();
                var peselStudent = context.Students.Where(x => x.PESEL == peselTextBox.Text).FirstOrDefault();

                if (peselTeacher != null || peselStudent != null || peselGuardian != null)
                {
                    MessageBox.Show("Istnieje już osoba o takim peselu!");
                    peselTextBox.Focus();
                }
                else
                {
                    context.Guardians.Add(guardian);
                    context.LoginsPasswords.Add(login);
                    context.SaveChanges();
                    MessageBox.Show("Opiekun dodany");
                    buttonAddWasClicked = false;
                    ClearTextBox();
                    DisableThings();
                    stackPanel1.IsEnabled = true;
                }
            }
        }
        private void DeleteGuardian()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var guardianToDelete = context.Guardians.Where(x => x.PESEL == peselTextBox.Text);

                if (guardianToDelete.FirstOrDefault() != null)
                {
                    context.Guardians.Remove(guardianToDelete.FirstOrDefault());
                    context.SaveChanges();
                    MessageBox.Show("Opiekun usunięty!");
                    buttonDeleteWasClicked = false;
                    ClearTextBox();
                    DisableThings();
                    VisibleOnDelete();
                    stackPanel1.IsEnabled = true;
                }
                else
                {
                    MessageBox.Show("Nie znaleziono opiekuna o takim peselu");
                    peselTextBox.Focus();
                }
            }
        }
        private void EditGuardian()
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var guardianToEdit = context.Guardians.Where(x => x.PESEL == searchTextBox.Text).SingleOrDefault();

                guardianToEdit.FirstName = firstNameTextBox.Text;
                guardianToEdit.LastName = lastNameTextBox.Text;
                guardianToEdit.PESEL = peselTextBox.Text;



                context.SaveChanges();
                ClearTextBox();
                DisableThings();
                VisibleOnDelete();
                stackPanel1.IsEnabled = true;
                buttonEditWasClicked = false;
                MessageBox.Show("Zaktualizowano!");
            }
        }

        private void searchTextBox_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            searchTextBox.Clear();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var guardianToEdit = context.Guardians.Where(x => x.PESEL == searchTextBox.Text).SingleOrDefault();

                if (guardianToEdit != null)
                {
                    VisibleOffEdit();
                    saveButton.IsEnabled = true;
                    clearButton.IsEnabled = true;

                    firstNameTextBox.Text = guardianToEdit.FirstName;
                    lastNameTextBox.Text = guardianToEdit.LastName;
                    peselTextBox.Text = guardianToEdit.PESEL;
                }
                else
                {
                    MessageBox.Show("Nie znaleziono opiekuna o podanym peselu!");
                    searchTextBox.Focus();
                }
            }
        }
    }
}
