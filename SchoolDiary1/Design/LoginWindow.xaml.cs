﻿using SchoolDiary1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace SchoolDiary1.Design
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        MainWindow main;
        Guardians guard = new Guardians();
        Teachers teach = new Teachers();
        Students stud = new Students();
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void Logbutton_Click(object sender, RoutedEventArgs e)
        {
            using (SchoolDiaryContext context = new SchoolDiaryContext())
            {
                var teacher = context.Teachers.Where(x => x.PESEL == LoginTextBox.Text && x.LoginPassword.Password == PasswordBox.Password).SingleOrDefault();
                var guardian = context.Guardians.Where(x => x.PESEL == LoginTextBox.Text && x.LoginPassword.Password == PasswordBox.Password).SingleOrDefault();
                var student = context.Students.Where(x => x.PESEL == LoginTextBox.Text && x.LoginPassword.Password == PasswordBox.Password).SingleOrDefault();

                if (TeacherRadioButton.IsChecked == true && teacher != null)
                {
                    main = new MainWindow(teach);
                    main.Show();
                    this.Close();
                }
                else if (GuardianRadioButton.IsChecked == true && guardian != null)
                {
                    main = new MainWindow(guard);
                    main.Show();
                    this.Close();
                }
                else if (StudentRadioButton.IsChecked == true && student != null)
                {
                    main = new MainWindow(stud);
                    main.Show();
                    this.Close();
                }
                else if (AdminRadioButton.IsChecked == true)
                {
                    main = new MainWindow();
                    main.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Błędny login/hasło");
                    LoginTextBox.Focus();
                }

            }
        }

        private void RegLabel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            RegistrationWindow regWindow = new RegistrationWindow();
            regWindow.Show();
        }
    }
}
