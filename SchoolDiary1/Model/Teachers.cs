﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDiary1.Model
{
    public enum Degree
    {
        mgr,
        dr,
        prof,
        hab
    };

    public class Teachers
    {
        public Teachers()
        {
            Classes = new HashSet<Classes>();
            NewsLetters = new HashSet<NewsLetter>();
            Notes = new HashSet<Notes>();
            CardSubjects = new HashSet<CardSubjects>();
            ClassesSubjects = new HashSet<ClassesSubjects>();
        }
        [Key, ForeignKey("LoginPassword")]
        public int TeachersId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string PESEL { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public Degree Degree { get; set; }
        public string IdentityCardNumber { get; set; }

        //public int AddressesId { get; set; }

        public virtual ICollection<Classes> Classes { get; set; }
        public virtual ICollection<NewsLetter> NewsLetters { get; set; }
        public virtual ICollection<Notes> Notes { get; set; }
        public virtual ICollection<CardSubjects> CardSubjects { get; set; }
        public virtual ICollection<ClassesSubjects> ClassesSubjects { get; set; }

        //[ForeignKey("AddressesId")]
        public virtual Addresses Address { get; set; }
        public virtual LoginsPasswords LoginPassword { get; set; }
    }
}
