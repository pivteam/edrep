﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDiary1.Model
{
    public class Subjects
    {
        public Subjects()
        {
            ClassesSubjects = new HashSet<ClassesSubjects>();
            CardSubjects = new HashSet<CardSubjects>();
        }
        [Key]
        public int SubjectsId { get; set; }
        public string SubjectName { get; set; }

        public virtual ICollection<ClassesSubjects> ClassesSubjects { get; set; }
        public virtual ICollection<CardSubjects> CardSubjects { get; set; }
    }
}
