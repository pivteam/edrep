﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDiary1.Model
{
    public class Students
    {
        public Students()
        {
            Attendances = new HashSet<Attendances>();
            CardSubjects = new HashSet<CardSubjects>();
            Notes = new HashSet<Notes>();
            Guardians = new HashSet<Guardians>();
        }
        [Key, ForeignKey("LoginPassword")]
        public int StudentsId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string PESEL { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string SchoolIdentityCard { get; set; }

        //public int ClassesId { get; set; }
        //public int AddressesId { get; set; }

        public virtual ICollection<Guardians> Guardians { get; set; }

       // [ForeignKey("ClassesId")]
        public virtual Classes Class { get; set; }
        public virtual ICollection<Notes> Notes { get; set; }
        public virtual ICollection<Attendances> Attendances { get; set; }
        public virtual ICollection<CardSubjects> CardSubjects { get; set; }

        //[ForeignKey("AddressesId")]
        public virtual Addresses Address { get; set; }
        public virtual LoginsPasswords LoginPassword { get; set; }
    }
}
